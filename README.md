![](https://img.shields.io/badge/Microverse-blueviolet)

# Leaderboard

> In this project, I set up a JavaScript project for the Leaderboard list app, using webpack and ES6. I developed the app following a wireframe.

## Built With

- HTML
- CSS
- Javascript
- Webpack

## Getting Started

- To get a local copy up and running follow these simple example steps:

### Steps

- To clone the project select the desired directory in cmd and run: `git clone git@github.com:demix007/Leaderboard.git`
- Install node.js, Install NPM linters for HTML/CSS/JS: `https://github.com/microverseinc/linters-config`
- For StyLint test, run: `npx stylelint "**/*.{css,scss}"`
- For WebHint test, run: `npx hint .`
- For EsLint test, run: `npx eslint .`
- To install the modules and dependencies listed, run: `npm install`

### Usage

- Open the index.html with a live server.

👤 **Ayodeji Abidemi**

- GitHub: [@githubhandle](https://github.com/demix007)
- Twitter: [@twitterhandle](https://twitter.com/dat_dope_demix)
- LinkedIn: [LinkedIn](https://linkedin.com/in/ayodeji-abidemi-b76935218/)

## 🤝 Contributing

Contributions, issues, and feature requests are welcome!

Feel free to check the [issues page](https://github.com/demix007/Leaderboard/issues).

## Show your support

Give a ⭐️ if you like this project!

## Acknowledgments

- Inspiration for this design was provided in the project requirements by Microverse.  

## 📝 License

This project is [MIT](https://github.com/demix007/Leaderboard/blob/main/LICENSE) licensed.

_NOTE: we recommend using the [MIT license](https://choosealicense.com/licenses/mit/) - you can set it up quickly by [using templates available on GitHub](https://docs.github.com/en/communities/setting-up-your-project-for-healthy-contributions/adding-a-license-to-a-repository). You can also use [any other license](https://choosealicense.com/licenses/) if you wish._













![](https://img.shields.io/badge/Microverse-blueviolet)

# 📗 Table of Contents

- [📖 About the Project](#about-project)
- [🛠 Built With](#built-with)
  - [Tech Stack](#tech-stack)
- [Key Features](#key-features)
- [🚀 Live Demo](#live-demo)
- [💻 Getting Started](#getting-started)
  - [Setup](#setup)
  - [Prerequisites](#prerequisites)
- [👥 Authors](#authors)
  - [🤝 Contributing](#contributing)
  - [⭐️ Show your support](#support)
  - [🙏 Acknowledgements](#acknowledgements)
- [📝 License](#license)



# 📖 BOOKSTORE <a name="about-project"></a>

**BOOKSTORE** is a website that displays scores submitted by different players. It also allows you to submit your score. All data is preserved thanks to the external Leaderboard API service. using webpack and ES6. I developed the app following a wireframe.

# 🛠 Built With <a name="built-with"></a>
 > This project was built with; 
- HTML 
- CSS
- Javascript
- Webpack
- REST API

# Tech Stack <a name="tech-stack"></a>
> ### Technologies used 
- GitHub 
- Visual Studio Code 
- GitBash
- Webpack
<details> 
<summary>Live Link</summary>
  <ul>
    <li><a href="https://demix007.github.io/Leaderboard/">Leaderboard</a></li>
  </ul>
</details>

# Key Features <a name="key-features"></a>
- **[Displays a board of scores]**
- **[Adds new score(s) to the board]**
- **[Removes score(s) from the board**

<p align="right">(<a href="#readme-top">back to top</a>)</p>

# 💻 Getting Started <a name="getting-started"></a>
> In order to use the project files, Kindly follow these steps:

## Steps
- To clone the project select the desired directory in cmd and run: `git@github.com:demix007/Leaderboard.git`
- Install node.js, Install NPM linters for HTML/CSS/JS: `https://github.com/microverseinc/linters-config`
- To check the live preview: `open the index.html file in the build folder with a live server`
- For StyLint test, run: `npx stylelint "**/*.{css,scss}"`
- For WebHint test, run: `npx hint .`
- For EsLint test, run: `npx eslint .`

## Prerequisites
- Have basic level knowledge about HTML/CSS/JS/ES6
- Have basic level knowledge about linters/Webpack
- Have basic level knowledge about the working of website

# Author
👤 **Ayodeji Abidemi**

[GitHub](https://github.com/demix007) | [Twitter](https://twitter.com/dat_dope_demix) | [LinkedIn](https://www.linkedin.com/in/ayodeji-abidemi-b76935218)

## 🤝 Contributing <a name="contributing"></a>
- Contributions, issues, and feature requests are welcome!

- Feel free to check the issues page. https://github.com/demix007/Leaderboard/issues   

## ⭐️ Show your support <a name="support"></a>
Give a ⭐️ if you like this project!

## 🙏 🙏 Acknowledgments <a name="acknowledgements"></a>
- I would like to thank [@microverseinc](https://github.com/microverseinc) for the templates and insights 

<p align="right">(<a href="#readme-top">back to top</a>)</p>

### 📝 License <a name="license"></a>
> Copyright 2021, [Ayodeji Abidemi]

> Permission is hereby granted, free of charge, to any person obtaining a copy of this [LEADERBOARD] and associated documentation files, to deal in the [LEADERBOARD] without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the [LEADERBOARD], and to permit persons to whom the [LEADERBOARD] is furnished to do so, subject to the following conditions:

> The above copyright notice and this permission notice shall be included in all copies or substantial portions of the [LEADERBOARD].

> THE [LEADERBOARD] IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE [LEADERBOARD] OR THE USE OR OTHER DEALINGS IN THE [LEADERBOARD].

<p align="right">(<a href="#readme-top">back to top</a>)</p>